For storing billions of entries a distributed in-memory solution is recommendable.
The cuisine-customer map would have to be partitioned for this (One node could be responsible for a subset of cuisines and customer-surnames).
The partitions should be distributed on different cluster nodes.
I would use an open source solution like "Apache Ignite" for this.