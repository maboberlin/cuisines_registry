package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.api.CuisinesRegistry;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.CuisineImpl;
import de.quandoo.recruitment.registry.model.Customer;
import de.quandoo.recruitment.registry.model.CustomerImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class InMemoryCuisinesRegistryTest {

    private CuisinesRegistry cuisinesRegistry;

    @Before
    public void setUp() {
        cuisinesRegistry = new InMemoryCuisinesRegistry();
    }

    @Test
    public void test_register_successful() {
        cuisinesRegistry.register(new CustomerImpl("1"), new CuisineImpl("fr", "french"));
        cuisinesRegistry.register(new CustomerImpl("2"), new CuisineImpl("ge", "german"));
        cuisinesRegistry.register(new CustomerImpl("3"), new CuisineImpl("it", "italian"));

        Assert.assertEquals(1, cuisinesRegistry.cuisineCustomers(new CuisineImpl("fr", null)).size());
        Assert.assertEquals(1, cuisinesRegistry.cuisineCustomers(new CuisineImpl("ge", null)).size());
        Assert.assertEquals(1, cuisinesRegistry.cuisineCustomers(new CuisineImpl("it", null)).size());

        cuisinesRegistry.register(new CustomerImpl("4"), new CuisineImpl("it", null));
        cuisinesRegistry.register(new CustomerImpl("5"), new CuisineImpl("it", null));

        Assert.assertEquals(1, cuisinesRegistry.cuisineCustomers(new CuisineImpl("fr", null)).size());
        Assert.assertEquals(1, cuisinesRegistry.cuisineCustomers(new CuisineImpl("ge", null)).size());
        Assert.assertEquals(3, cuisinesRegistry.cuisineCustomers(new CuisineImpl("it", null)).size());
    }

    @Test
    public void test_register_badvalues() {
        cuisinesRegistry.register(null, null);
        cuisinesRegistry.register(new CustomerImpl("1"), null);
        cuisinesRegistry.register(null, new CuisineImpl("ge", "german"));
        cuisinesRegistry.register(new CustomerImpl(""), new CuisineImpl("ge", "german"));
        cuisinesRegistry.register(new CustomerImpl("2"), new CuisineImpl("", "german"));

        Assert.assertEquals(0, cuisinesRegistry.topCuisines(10).size());
    }

    @Test
    public void test_cuisinecustomers_badvalues() {
        cuisinesRegistry.register(new CustomerImpl("1"), new CuisineImpl("fr", "french"));
        cuisinesRegistry.register(new CustomerImpl("2"), new CuisineImpl("ge", "german"));
        cuisinesRegistry.register(new CustomerImpl("3"), new CuisineImpl("it", "italian"));

        Assert.assertEquals(0, cuisinesRegistry.cuisineCustomers(null).size());
        Assert.assertEquals(0, cuisinesRegistry.cuisineCustomers(new CuisineImpl(null, null)).size());
        Assert.assertEquals(0, cuisinesRegistry.cuisineCustomers(new CuisineImpl("asdasdasd", null)).size());
    }

    @Test
    public void test_register_validationerror() {
        cuisinesRegistry.register(new CustomerImpl("1"), new CuisineImpl("xyz", "xyz"));

        Assert.assertEquals(0, cuisinesRegistry.cuisineCustomers(new CuisineImpl("xyz", "xyz")).size());
    }

    @Test
    public void test_multiplecuisines_successfully() {
        cuisinesRegistry.register(new CustomerImpl("1"), new CuisineImpl("fr", "french"));
        cuisinesRegistry.register(new CustomerImpl("1"), new CuisineImpl("ge", "german"));
        cuisinesRegistry.register(new CustomerImpl("1"), new CuisineImpl("it", "italian"));

        Assert.assertEquals(1, cuisinesRegistry.cuisineCustomers(new CuisineImpl("fr", null)).size());
        Assert.assertEquals(1, cuisinesRegistry.cuisineCustomers(new CuisineImpl("ge", null)).size());
        Assert.assertEquals(1, cuisinesRegistry.cuisineCustomers(new CuisineImpl("it", null)).size());
        Assert.assertEquals(3, cuisinesRegistry.customerCuisines(new CustomerImpl("1")).size());
    }

    @Test
    public void test_topcuisines_successfully() {
        cuisinesRegistry.register(new CustomerImpl("1"), new CuisineImpl("fr", "french"));
        cuisinesRegistry.register(new CustomerImpl("2"), new CuisineImpl("ge", "german"));
        cuisinesRegistry.register(new CustomerImpl("3"), new CuisineImpl("it", "italian"));
        cuisinesRegistry.register(new CustomerImpl("4"), new CuisineImpl("fr", "french"));
        cuisinesRegistry.register(new CustomerImpl("5"), new CuisineImpl("it", "italian"));
        cuisinesRegistry.register(new CustomerImpl("6"), new CuisineImpl("fr", "french"));

        Assert.assertEquals(1, cuisinesRegistry.topCuisines(1).size());
        Assert.assertEquals(new CuisineImpl("fr", null), cuisinesRegistry.topCuisines(1).get(0));
    }

}