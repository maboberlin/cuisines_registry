package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.api.CuisinesRegistry;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@Slf4j
public class InMemoryCuisinesRegistry implements CuisinesRegistry {

    private static final Set<String> VALID_CUISINE_NAMES = new HashSet<>(Arrays.asList("italian", "french", "german"));

    private Map<Cuisine, Set<Customer>> cuisineMap = new ConcurrentHashMap<>();

    @Override
    public void register(final Customer customer, final Cuisine cuisine) {
        if (customer == null || cuisine == null || StringUtils.isEmpty(customer.getUuid()) || StringUtils.isEmpty(cuisine.getUuid())) {
            log.warn("Registering customer-cuisine relation requires customer and cuisine parameters with valid uuids. " +
                     "Passed: {} [customer], {} [cuisine]", customer, cuisine);
            return;
        }

        if (!cuisineMap.containsKey(cuisine)) {
            List<String> validationErrors = this.validateCuisine(cuisine);
            if (!validationErrors.isEmpty()) {
                log.warn("Cuisine could not be registered due to validation errors: {}", validationErrors);
                return;
            }
            cuisineMap.put(cuisine, new HashSet<>(Arrays.asList(customer)));
            log.info("Registered new cuisine: {}", cuisine);
        } else {
            cuisineMap.get(cuisine).add(customer);
        }
        log.info("Added customer [{}] to cuisine [{}]", customer, cuisine);
    }

    @Override
    public List<Customer> cuisineCustomers(final Cuisine cuisine) {
        return Optional.ofNullable(cuisine)
           .map(cuisineMap::get)
           .map(customers -> Collections.unmodifiableList(new ArrayList<>(customers)))
           .orElse(Collections.emptyList());
    }

    @Override
    public List<Cuisine> customerCuisines(final Customer customer) {
        return cuisineMap.entrySet().stream()
            .filter(el -> el.getValue().contains(customer))
            .map(Map.Entry::getKey)
            .collect(Collectors.toList());
    }

    @Override
    public List<Cuisine> topCuisines(final int n) {
        return cuisineMap.entrySet().stream()
            .sorted((el1, el2) -> el2.getValue().size() - el1.getValue().size())
            .limit((long)n)
            .map(Map.Entry::getKey)
            .collect(Collectors.toList());

    }

    private List<String> validateCuisine(Cuisine cuisine) {
        List<String> errors = new ArrayList<>();
        // validate cuisine name
        if (!VALID_CUISINE_NAMES.contains(cuisine.getName())) {
            errors.add("Name of cuisine to register is not among valid cuisine names");
        }
        return errors;
    }
}
