package de.quandoo.recruitment.registry.model;

import lombok.*;

@Getter
@Setter
@ToString
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@AllArgsConstructor
public class CuisineImpl implements Cuisine {

    @EqualsAndHashCode.Include
    private final String uuid;

    @EqualsAndHashCode.Exclude
    private final String name;
}
