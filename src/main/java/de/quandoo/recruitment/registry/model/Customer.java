package de.quandoo.recruitment.registry.model;

public interface Customer {

    public String getUuid();
}
