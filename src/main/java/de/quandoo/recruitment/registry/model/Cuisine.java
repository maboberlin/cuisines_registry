package de.quandoo.recruitment.registry.model;

public interface Cuisine {

    public String getUuid();

    public String getName();
}
