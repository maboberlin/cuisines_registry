package de.quandoo.recruitment.registry.model;

import lombok.*;

@Getter
@Setter
@ToString
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@AllArgsConstructor
public class CustomerImpl implements Customer {

    @EqualsAndHashCode.Include
    private final String uuid;

}
